package com.safaricom.microservices.msmovies.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.safaricom.microservices.msmovies.model.JsonResponse;
import com.safaricom.microservices.msmovies.model.Movie;
import com.safaricom.microservices.msmovies.model.Rating;
import com.safaricom.microservices.msmovies.model.Request;
import com.safaricom.microservices.msmovies.model.Watchlist;
import com.safaricom.microservices.msmovies.repository.MoviesRepository;
import com.safaricom.microservices.msmovies.repository.RatingRepository;
import com.safaricom.microservices.msmovies.repository.WatchlistRepository;
import com.safaricom.microservices.msmovies.util.ConfigProperties;

import io.swagger.annotations.ApiOperation;

@RestController
public class MoviesController {

	private Logger logger = LoggerFactory.getLogger(MoviesController.class);

	@Autowired
	ConfigProperties config;

	@Autowired
	MoviesRepository movieRepo;

	@Autowired
	RatingRepository ratingRepo;

	@Autowired
	WatchlistRepository watchlistRepo;

	@ApiOperation(value = "This endpoint for registering movie", response = JsonResponse.class)
	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> register(@RequestBody Request body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getTitle() == null || body.getDescription() == null || body.getCreatedby() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=itle, Description and Createdby are required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("Title, Description and Createdby are required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		if (movieRepo.findByTitle(body.getTitle()) != null) {
			response.setRes_code("409");
			response.setRes_msg("Movie already registered");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=409 | ResponseMsg=Conflict | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.CONFLICT);
		}
		try {
			Movie movie = new Movie();
			movie.setTitle(body.getTitle());
			movie.setDescription(body.getDescription());
			movie.setCreatedby(body.getCreatedby());
			movie.setStatus(1);
			movieRepo.save(movie);
			logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("200");
			response.setRes_msg("Registration done successfully");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			response.setRes_code("400");
			response.setRes_msg("Bad Request");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=500 | ResponseMsg=Bad Request | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

	}

	@ApiOperation(value = "This endpoint for editing movie", response = JsonResponse.class)
	@PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> edit(@RequestBody Request body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getMovieid() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=Movieid is required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("movieid is required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			Movie movie = movieRepo.findById(body.getMovieid()).get();
			if (body.getTitle() != null) {
				movie.setTitle(body.getTitle());
			}
			if (body.getCreatedby() != null) {
				movie.setCreatedby(body.getCreatedby());
			}
			if (body.getDescription() != null) {
				movie.setDescription(body.getDescription());
			}
			movieRepo.save(movie);
			logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("200");
			response.setRes_msg("movie edited successfully");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			response.setRes_code("400");
			response.setRes_msg("Bad Request");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=500 | ResponseMsg=Bad Request | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		}
	}

	@ApiOperation(value = "This endpoint for deleting movie", response = JsonResponse.class)
	@PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> delete(@RequestBody Request body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getMovieid() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=Movieid is required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("movieid is required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			Movie movie = movieRepo.findById(body.getMovieid()).get();
			movie.setStatus(3);
			movieRepo.save(movie);
			logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("200");
			response.setRes_msg("movie deleted successfully");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			response.setRes_code("400");
			response.setRes_msg("Bad Request");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=500 | ResponseMsg=Bad Request | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		}
	}

	@ApiOperation(value = "This endpoint for rating movie", response = JsonResponse.class)
	@PostMapping(value = "/rate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> rate(@RequestBody Request body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getMovieid() == null || body.getUserid() == null || body.getRate() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=movieid, userid and rate are required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("movieid, userid and rate are required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			if(movieRepo.findById(body.getMovieid()).get().getStatus()==0) {
				logger.error(
						"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=movieid and userid are required | Headers={} |",
						referenceId, timestamp, httpHeaders);
				response.setRes_code("400");
				response.setRes_msg("movieid and userid are required");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			Rating rate = new Rating();
			rate.setMovieid(body.getMovieid());
			rate.setRate(body.getRate());
			rate.setUserid(body.getUserid());
			ratingRepo.save(rate);
			logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("200");
			response.setRes_msg("movie rated successfully");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			response.setRes_code("400");
			response.setRes_msg("Bad Request");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=500 | ResponseMsg=Bad Request | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		}
	}

	@ApiOperation(value = "This endpoint for updating movie watchlist", response = JsonResponse.class)
	@PostMapping(value = "/watch", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> watchlist(@RequestBody Request body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getMovieid() == null || body.getUserid() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=movieid and userid are required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("movieid and userid are required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			if(movieRepo.findById(body.getMovieid()).get().getStatus()==0) {
				logger.error(
						"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=403 | ResponseMsg=movieid and userid are required | Headers={} |",
						referenceId, timestamp, httpHeaders);
				response.setRes_code("400");
				response.setRes_msg("movieid and userid are required");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			Watchlist watch=new Watchlist();
			watch.setMovieid(body.getMovieid());
            watch.setUserid(body.getUserid());
            watchlistRepo.save(watch);
            logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("200");
			response.setRes_msg("Success");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			response.setRes_code("400");
			response.setRes_msg("Bad Request");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=500 | ResponseMsg=Bad Request | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		}
	}
	@ApiOperation(value = "This endpoint for getting all movies", response = JsonResponse.class)
	@PostMapping(value = "/all", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAll(@RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
            logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=Movie | SourceSystem=MovieService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(movieRepo.findByStatus(1), HttpStatus.OK);
	
	}
}
