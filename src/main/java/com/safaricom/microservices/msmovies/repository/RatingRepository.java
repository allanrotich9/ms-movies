package com.safaricom.microservices.msmovies.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.microservices.msmovies.model.Rating;



public interface RatingRepository  extends JpaRepository<Rating, Integer>{

}
