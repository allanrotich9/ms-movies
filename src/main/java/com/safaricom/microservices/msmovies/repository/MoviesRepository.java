package com.safaricom.microservices.msmovies.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.microservices.msmovies.model.Movie;


public interface MoviesRepository extends JpaRepository<Movie, Integer>{
 Movie findByTitle(String title);
 Movie findByStatus(Integer status);
}
