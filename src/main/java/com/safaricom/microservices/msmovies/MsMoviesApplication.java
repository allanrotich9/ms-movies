package com.safaricom.microservices.msmovies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsMoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMoviesApplication.class, args);
	}

}
