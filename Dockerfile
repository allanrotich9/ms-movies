FROM openjdk:8-jre-alpine

LABEL maintainer="AKROTICH@safaricom.co.ke"

EXPOSE 9200

VOLUME /tmp

ADD target/example-0.0.1-SNAPSHOT.jar example.jar

RUN /bin/sh -c 'touch /example.jar'

ENTRYPOINT ["java","-Xmx256m","-Djava.security.egd=file:/dev/./urandom","-jar","/example.jar"]
